'''
Created on 2014-10-10

@author: charlie
'''

import vpnClient
import vpnServer

"""
    vpnTest was used to test the overall vpn via CLI.
    Used for debugging the VPN system ONLY. All executions 
    happen in vpnGUI.
"""
def main():
    
    choice = None
    
    while(choice != "client" and choice != "server" and choice != "c" and choice != "s"):
        choice = raw_input("client -or- server:")
    
    if choice == "client":
        c_addr = raw_input("client address: ")
        c_port = raw_input("client port: ")
        s_addr = raw_input("server address: ")
        s_port = raw_input("server port: ")
        secret = raw_input("secret password: ")
    
        client_object = vpnClient.vpnClient(c_addr, c_port, s_addr, s_port, secret)
        client_object.runClient()

    elif choice == "c":
        c_addr = 'localhost'
        c_port = 8889
        s_addr = 'localhost'
        s_port = 8888
        secret = "Unknown"
        client_object = vpnClient.vpnClient(c_addr, c_port, s_addr, s_port, secret)
        client_object.runClient()

    elif choice == "server":
        s_addr = raw_input("server address: ")
        s_port = raw_input("server port: ")
        secret = raw_input("secret password: ")
        
        server_object = vpnServer.vpnServer(s_addr, s_port, secret)
        server_object.runServer()

    elif choice == "s":
        s_addr = 'localhost'
        s_port = 8888
        secret = "Unknown"
        server_object = vpnServer.vpnServer(s_addr, s_port, secret)
        server_object.runServer()

if __name__ == '__main__':
    main()