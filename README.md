# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Introduction:
PyCharm is the IDE that we used to develop and run this VPN. To run the VPN program, you must build and run vpnGui.py. Running anyother codes will not launch the VPN program. This VPN has been tested on Windows machines running Python 2.7. Although tested on OSX machines, we found that OSX platforms will produce graphical glitches exclusive to OSX.

The VPN has three main areas. The connection settings are set at the top, the text output is in the middle and the text entry area is on the bottom. The sever must be setup first because the client will return an error if it is unable to connect to the server. The user can select to use the VPN as a server or as a client. There are also text boxes to enter IP addresses, port numbers and the secret key. Once all of the settings have been double-checked the user can click on the connect button. If the VPN is in client mode the client will connect to a server at the specified IP address. If the VPN is server mode it will start listening for connections on the specified port. The connect button will change to a disconnect button once a connection has been established.