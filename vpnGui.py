'''
Created on 2014-10-07

@author: Charlie
@editor: Harlen 
@contributor: Ehsan


Last Edit: October 14th 7:36pm
'''
from  Tkinter import *
import tkMessageBox
import vpnClient
import vpnServer
import threading
import time

class vpnGui(Frame):
    
    def __init__(self, root):
        Frame.__init__(self, root)
        self.root = root
        self.initUI()
        self.CorS =None


    def initUI(self):

        # setup/modify the root window
        self.root.title("Simple VPN")
        ScreenSizeX = self.root.winfo_screenwidth()
        ScreenSizeY = self.root.winfo_screenheight()
        self.FrameSizeX  = 780
        self.FrameSizeY  = 650
        FramePosX   = (ScreenSizeX - self.FrameSizeX)/2
        FramePosY   = (ScreenSizeY - self.FrameSizeY)/2
        # sets the size of the window
        self.root.geometry("%sx%s+%s+%s" % (self.FrameSizeX,self.FrameSizeY,FramePosX,FramePosY))
        self.root.resizable(width=True, height=True)
        padX = 10
        padY = 10
        parentFrame = Frame(self.root)
        parentFrame.grid( stick=E+W+N+S)
        ipGroup = Frame(parentFrame)

        #clientIPTextbox
        self.clientIPVar = StringVar()
        self.clientIPVar.set("Client IP")
        clientIPField = Entry(ipGroup, width=15, textvariable=self.clientIPVar)
        clientIPField.grid(row=1, column=2)

        #clientPortTextbox
        self.clientPortVar = StringVar()
        self.clientPortVar.set("Client Port")
        clientPortField = Entry(ipGroup, width=10, textvariable=self.clientPortVar)
        clientPortField.grid(row=1, column=3)

        # connectionButton
        self.connectionButton = Button(ipGroup, text="Connect", width=10, command=self.connect)
        self.connectionButton.grid(row=3, column=3)

        #serverIPTextbox
        self.serverIPVar = StringVar()
        self.serverIPVar.set("Server IP")
        serverIPField = Entry(ipGroup, width=15, textvariable=self.serverIPVar)
        serverIPField.grid(row=0, column=2)

        #serverPortTextbox
        self.serverPortVar = StringVar()
        self.serverPortVar.set("Server Port")
        serverPortField = Entry(ipGroup, width=10, textvariable=self.serverPortVar)
        serverPortField.grid(row=0, column=3)

        #Radio Buttons
        self.VarServer = IntVar()
        self.VarClient = IntVar()
        self.RB_Server = Radiobutton(ipGroup, text="Run as Server", variable=self.VarServer, value=1,command=self.ServerRadio)
        self.RB_Client = Radiobutton(ipGroup, text="Run as Client", variable=self.VarClient, value=1,command=self.ClientRadio)
        self.RB_Server.grid(row=0, column=0)
        self.RB_Client.grid(row=1, column=0)

        #Secret Key Label
        secretKeyLabel = Label(ipGroup, text="Enter Shared Secret Key: ")
        secretKeyLabel.grid(row=3, column=0)

        #secretkeyTextbox
        self.secretKeyVar = StringVar()
        self.secretKeyVar.set("Shared Secret")
        secretKeyField = Entry(ipGroup, width=15, textvariable=self.secretKeyVar)
        secretKeyField.grid(row=3, column=2)


        #self.scrollbar = Scrollbar(self.root)
        #self.scrollbar.pack(side = RIGHT, fill = Y)
        #We had a Frame for Debug messages and
        #chat window and debug
        readChatGroup = Frame(parentFrame)
        readChatGroup.pack(expand =True)
        #self.Debug_TextBox = Text(readChatGroup, bg="white",yscrollcommand = self.scrollbar.set ,width=60, height=30, state=DISABLED)
        self.Debug_TextBox = Text(readChatGroup, bg="white" ,width=48, height=30)
        self.History_textbox = Listbox(readChatGroup, bg="white", width=64, height=30)

        self.Debug_TextBox.pack(expand = True,side =LEFT)
        self.History_textbox.pack(expand = True,side = LEFT)
        readChatGroup.grid(row=1, column=0)
        #self.History_textbox .pack( side = LEFT, fill = BOTH )
        #self.scrollbar.config( command = self.History_textbox.yview )

        writeChatGroup = Frame(parentFrame)
        self.chatVar = StringVar()

        #Chatfield is where the client and server put their messages in
        self.chatField = Entry(writeChatGroup, width=80, textvariable=self.chatVar)
        self.chatField.grid(row=0, column=0, sticky=W,pady = 10)
        bottomLabel = Label(parentFrame, text="EECE 412 : Group 18")
        ipGroup.grid(row=0, column=0)
        writeChatGroup.grid(row=2, column=0, pady=10)
        bottomLabel.grid(row=3, column=0, pady=0)

        #sendButton
        sendChatButton = Button(writeChatGroup, text="Send", width=10,command=self.sendMessage)
        sendChatButton.grid(row=0, column=2, padx=5)
        """

        LogButton = Button(writeChatGroup, text="Log output", width=10,command=self.log)
        LogButton.grid(row=1, column=2, padx=5)
        """


    """
    The next couple of function are self explanatory. We would just put our functions in them
    """
    def connect(self):
        if self.connectionButton["text"] == "Connect":
            self.connectionButton["text"] = "..."
            if (self.CorS == 1):

                c_addr = self.clientIPVar.get()
                c_port = self.clientPortVar.get()
                s_addr = self.serverIPVar.get()
                s_port = self.serverPortVar.get()
                secret = self.secretKeyVar.get()
                #create one recv socket (server sock) for server
                try:
                    self.server_object = vpnServer.vpnServer(s_addr, s_port, secret)
                    self.server_object.prepare_socket(1)
                except:
                    tkMessageBox.showinfo("Error creating a socket", "Please enter a valid IP address")
                    self.connectionButton["text"] = "Connect"
                    return
                self.server_object.accept_connection()
                ##############self.server_object.output_obj.get_debug()
                if self.server_object.authenticate_client() == False:
                    self.server_object.close_client()
                    tkMessageBox.showinfo("Authentication Error", "The Secret key does not match.")
                self.server_object.update_key(self.server_object.keygen_obj.getSessionKey())

                self.server_debug_thread_stop_event = threading.Event()
                self.server_debug_thread = threading.Thread(target=self.Server_thread_func, args=(self.server_debug_thread_stop_event,))
                self.server_debug_thread.start()

                #This section was used to update the history TextBox in GUI
                self.server_history_thread_stop_event = threading.Event()
                self.server_history_thread = threading.Thread(target=self.Server_thread_hist, args=(self.server_history_thread_stop_event,))
                self.server_history_thread.start()

                self.server_thread_stop_event = threading.Event()
                self.server_thread = threading.Thread(target=self.server_object.guiRecv, args=(self.server_thread_stop_event,))
                self.server_thread.start()


                self.connectionButton["text"] = "Disconnect"

            elif (self.CorS == 0):
                #Set this machine up as client
                c_addr = self.clientIPVar.get()
                c_port = self.clientPortVar.get()
                s_addr = self.serverIPVar.get()
                s_port = self.serverPortVar.get()
                secret = self.secretKeyVar.get()
                try:
                    self.client_object = vpnClient.vpnClient(c_addr, c_port, s_addr, s_port, secret)
                except:
                    tkMessageBox.showinfo("Error creating socket", "Please enter a valid IP address.")
                    return

                self.client_object.prepare_socket()

                self.client_object.connect_server()

                if self.client_object.authenticate_server() == False:
                    self.client_object.clientSock.close()
                    tkMessageBox.showinfo("Authentication Error", "The Secret key does not match.")
                    return
                self.client_object.update_key(self.client_object.keygen_obj.getSessionKey())


                #This section was used to update the debug TextBox in GUI
                self.client_debug_thread_stop_event = threading.Event()
                self.client_debug_thread = threading.Thread(target=self.Client_thread_func, args=(self.client_debug_thread_stop_event,))
                self.client_debug_thread.start()

                #This section was used to update the history TextBox in GUI
                self.client_history_thread_stop_event = threading.Event()
                self.client_history_thread = threading.Thread(target=self.Client_thread_hist, args=(self.client_history_thread_stop_event,))
                self.client_history_thread.start()


                self.client_thread_stop_event = threading.Event()
                self.client_thread = threading.Thread(target=self.client_object.guiRecv, args=(self.client_thread_stop_event,))
                self.client_thread.start()

                self.connectionButton["text"] = "Disconnect"
            else:
                tkMessageBox.showinfo("No mode selected", "Please select one of the modes.")
                self.connectionButton["text"] = "Connect"

        else:
            #for disconnecting
            if (self.CorS == 1): # if itz a server
                self.server_history_thread_stop_event.set()
                self.server_debug_thread_stop_event.set()
                self.server_thread_stop_event.set()
                self.server_object.close_client()
            if  (self.CorS == 0): # if itz a client
                self.client_debug_thread_stop_event.set()
                self.client_history_thread_stop_event.set()
                self.client_thread_stop_event.set()
                self.client_object.clientSock.close()
            self.connectionButton["text"] = "Connect"



    def Client_thread_func (self,stop_event):
        while ( not stop_event.is_set() ):
            self.Debug_TextBox.option_clear()
            if (self.client_object.output_obj.check_debug() == True ):
                self.Debug_TextBox.config(state=NORMAL)
                self.Debug_TextBox.insert(END,'\n'.join(map(str, self.client_object.output_obj.get_debug())))
                self.Debug_TextBox.config(state=DISABLED)
                self.client_object.output_obj.del_debug()

    def Client_thread_hist (self, stop_event):
        while ( not stop_event.is_set() ):
            if (self.client_object.output_obj.check_message() == True ):
                self.History_textbox.config(state=NORMAL)
                self.History_textbox.insert(END,'\n'.join(map(str, self.client_object.output_obj.get_message())))
                self.History_textbox.config(state=DISABLED)
                self.client_object.output_obj.del_message()

    def Server_thread_func (self,stop_event):
        while ( not stop_event.is_set() ):

            self.Debug_TextBox.option_clear()
            if (self.server_object.output_obj.check_debug() == True ):
                self.Debug_TextBox.config(state=NORMAL)
                self.Debug_TextBox.insert(END,'\n'.join(map(str, self.server_object.output_obj.get_debug())))
                self.Debug_TextBox.config(state=DISABLED)
                self.server_object.output_obj.del_debug()

    def Server_thread_hist (self, stop_event):
        while ( not stop_event.is_set() ):
            if (self.server_object.output_obj.check_message() == True ):
                self.History_textbox.config(state=NORMAL)
                self.History_textbox.insert(END,'\n'.join(map(str, self.server_object.output_obj.get_message())))
                self.History_textbox.config(state=DISABLED)
                self.server_object.output_obj.del_message()



    def sendMessage(self):
        message = self.chatField.get()
        try:
            if (self.CorS == 0):
                self.client_object.s_send_message(message)
                self.chatField.delete(0,END)
            if (self.CorS == 1):
                self.server_object.s_send_message(message)
                self.chatField.delete(0,END)
        except:
            tkMessageBox.showinfo("Connection Error", "Make sure you connect first.")

    def log (self):
        tkMessageBox.showinfo("Hello", "Loging outpy")

    def ServerRadio (self):
        self.VarClient.set(0)
        self.CorS = 1

    def ClientRadio(self):
        self.VarServer.set(0)
        self.CorS = 0


def main():

    # creates the window
    root = Tk()


    app = vpnGui(root)

    # kicks off the main event loop
    root.mainloop()

class bcolors:
    header = '\033[95m'
    blue = '\033[94m'
    green = '\033[92m'
    warning = '\033[93m'
    fail = '\033[91m'
    endc = '\033[0m'

if __name__ == '__main__':
  main()
