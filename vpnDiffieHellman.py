'''
Created on 2014-10-08

@author: charlie
'''

from binascii import hexlify
from Crypto.Random import random
from Crypto.Hash import SHA256

class vpnDiffieHellman(object):
    """
    Will be an object in the vpnKeygen class.
    
    This class is responsible for implementing the DiffieHellman key exchange between
    the client and the server. This method will create the private, public, and the 
    session keys that will be used throughout the VPN application.
    """

    prime = 340282366920938463463374607431768210659 #mutually agreed safe prime number
    base = 3                                        #mutually agreed base number
    
    privateKey = None   #a -or- b (exponent)
    publicKey = None    #(g^a mod p) -or- (g^b mod p)
    sharedKey = None    #(g^ab mod p)
    
    def __init__(self):
        """
        vpnDiffieHellman constructor.
            -    Generate the private and public key upon construction.
        """
        self.privateKey = self.genPrivateKey(256)
        self.publicKey = self.genPublicKey()

    def genPrivateKey(self, bits):
        """
        Generate the private key using a secure random number function.
        """
        return random.getrandbits(bits)

    def genPublicKey(self):
        """
        Generate the public key using g^a mod p.
        """
        return pow(self.base, self.privateKey, self.prime)

    def checkPublicKey(self, otherKey):
        """
        Check the other party's public key to make sure it's valid.
        Since a safe prime is used, verify that the Lagrange symbol is equal to one.
        Technique learned from:
        http://crypto.stackexchange.com/questions/2131/how-should-i-check-the-received-ephemeral-diffie-hellman-public-keys
        """
        if(otherKey > 2 and otherKey < self.prime - 1):
            if(pow(otherKey, (self.prime - 1)/2, self.prime) == 1):
                return True
        return False          
    
    def genSharedKey(self, otherKey):
        """
        Derive the shared secret, then hash it to obtain the shared key.
        """
        if(self.checkPublicKey(otherKey) == True):
            self.sharedSecret = pow(otherKey, self.privateKey, self.prime)
            key = SHA256.new()
            key.update(str(self.sharedSecret))
            self.sharedKey = key.digest()
        else:
            raise Exception("Invalid public key.")

#Section below was used for testing and debugging       
"""
    def getSharedKey(self):
        return self.sharedKey

if __name__=="__main__":
    
    a = vpnDiffieHellman()
    b = vpnDiffieHellman()
    
    print a.publicKey
    print b.publicKey
    
    a.genSharedKey(b.publicKey)
    b.genSharedKey(a.publicKey)

    if(a.getSharedKey() == b.getSharedKey()):
        print "Shared keys match."
        print "Key:", hexlify(a.sharedKey)
    else:
        print "Shared secrets didn't match!"
        print "Shared secret: ", a.genSharedKey(b.publicKey)
        print "Shared secret: ", b.genSharedKey(a.publicKey)
"""