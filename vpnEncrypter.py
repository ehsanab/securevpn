'''
Created on 2014-10-08

@author: Ehsan
'''

#used to generate random numbers
from ctypes import c_int16
import random
from Crypto.Cipher import DES
from Crypto.Cipher import AES
from Crypto import Random
import hashlib
import hmac
import binascii
import vpnKeygen
from binascii import hexlify

class vpnEncrypter(object):
    """
    Will be an object in the vpnClient and vpnServer class.
    
    This class is responsible for encrypting, dencrypting, hashing, and
    checking the integrity of the sent and received messages.
    """

    def __init__(self, key):
        """
        vpnEncrypter constructor.
            -   Stores the key that is passed in. This key will be used for
                all encrypting and decrypting.
        """
        self.key = key

    def generate_random_bytes(self, num_bytes):
        '''
        Function generates a random number of specified byte length.
        '''
        return Random.get_random_bytes(num_bytes)

    def get_key (self):
        '''
        Function fetches the key that is being used by the encrypter object.
        '''
        return self.key

    def encrypt_AES_CFB(self, plain_text ,iv):
        """
        This function encrypts the plain text using AES_CFB given the key along with the IV
        Note: The IV should be the one which was used to encrypt.
        """
        aes = AES.new(self.key, AES.MODE_CFB, iv)
        cipher_text = aes.encrypt(plain_text)
        return cipher_text

    def decrypt_AES_CFB (self, cipher_text, iv):
        """
        This function decrypts the cipher text using AES_CFB given the key along with the IV
        Note: The IV should be the one which was used to encrypt.
        """
        aess = AES.new(self.key, AES.MODE_CFB, iv)
        plain_text = aess.decrypt(cipher_text)
        return plain_text

    def convert_to_hex(self, encrypted_data):
        """
        Function fetches the hexadecimal representation of encrypted and hashed values.
        """
        return (binascii.hexlify(encrypted_data))

    def compute_HMAC (self, key, message):
        """
        Function computes the MAC of the passed in plain ext using SHA256 hashing.
        """
        mac = hmac.new(key, digestmod=hashlib.sha256)
        mac.update(message)
        return mac.digest()
    
    def compare_HMAC (self, mac_a, mac_b):
        """
        Function compares the value of two MAC. Return True if match, False otherwise.
        """
        if not mac_a == mac_b:
            return False
        return True

#Section below was used for testing and debugging       
"""
if __name__ == '__main__':
    
    keygen_object = vpnKeygen.vpnKeygen("secret")
    encrypter_object1 = vpnEncrypter(keygen_object.getSymmetricKey())
    encrypter_object2 = vpnEncrypter(keygen_object.getPublicKey())
    encrypter_object3 = vpnEncrypter(keygen_object.getPrivateKey())
    
    plain_text = "Helloworld!HELLOWOWLRDHELLOWLDORLD!!!!!!!A paragraph (from the Greek paragraphos, to write beside or written beside) is a self-contained unit of a discourse in writing dealing with a particular point or idea. A paragraph consists of one or more sentences."
    print('\nKey= '+ hexlify(encrypter_object1.get_key()))
    iv = encrypter_object1.generate_random_bytes(16)
    print ("IV:  " + encrypter_object1.convert_to_hex(iv))
    ctt = encrypter_object1.encrypt_AES_CFB(plain_text, iv)
    print ('Cipher text (AES_CFB):'+ encrypter_object1.convert_to_hex(ctt))
    print encrypter_object1.decrypt_AES_CFB(ctt, iv)
    
    plain_text = "Helloworld!HELLOWOWLRDHELLOWLDORLD!!!!!!!A paragraph (from the Greek paragraphos, to write beside or written beside) is a self-contained unit of a discourse in writing dealing with a particular point or idea. A paragraph consists of one or more sentences."
    print('\nKey= '+ hexlify(encrypter_object2.get_key()))
    iv = encrypter_object2.generate_random_bytes(16)
    print ("IV:  " + encrypter_object2.convert_to_hex(iv))
    ctt = encrypter_object2.encrypt_AES_CFB(plain_text, iv)
    print ('Cipher text (AES_CFB):'+ encrypter_object2.convert_to_hex(ctt))
    print encrypter_object2.decrypt_AES_CFB(ctt, iv)
    
    plain_text = "Helloworld!HELLOWOWLRDHELLOWLDORLD!!!!!!!A paragraph (from the Greek paragraphos, to write beside or written beside) is a self-contained unit of a discourse in writing dealing with a particular point or idea. A paragraph consists of one or more sentences."
    print('\nKey= '+ hexlify(encrypter_object3.get_key()))
    iv = encrypter_object3.random_bytes(16)
    print ("IV:  " + encrypter_object3.convert_to_hex(iv))
    ctt = encrypter_object3.encrypt_AES_CFB(plain_text, iv)
    print ('Cipher text (AES_CFB):'+ encrypter_object3.convert_to_hex(ctt))
    print encrypter_object3.decrypt_AES_CFB(ctt, iv)
"""    
    
    
