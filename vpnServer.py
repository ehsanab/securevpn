'''
Created on 2014-10-07

@author: Eshan 
@contributor: charlie
'''

from vpnEncrypter import vpnEncrypter
from vpnKeygen import vpnKeygen
from vpnOutput import vpnOutput
import socket
from _socket import IPPROTO_TCP
from Crypto.Hash import SHA256
import sys
import select
import binascii
from binascii import hexlify
import sys

from __builtin__ import False

class vpnServer(object):
    """
    Will be an object in the vpnGui class.
    
    This class is responsible for executing server functions.
    """
    keygen_obj = None
    encrypt_obj = None
    output_obj = None
    serverAddr = None
    serverPort = None
    serverSock = None
    clientAddr = None
    clientSock = None
    secretPass = None
    server = False
    connect = False
    DH_public = None
    DH_private = None
    DH_other = None
    client_nonce = 0
    server_nonce = 0

    
    def __init__(self, s_addr, s_port, secret):
        """
        vpnServer constructor.
            -    Stores the value entered from GUI
            -    Create and configure socket for server
            -    Create output, keygen, encrypter object
        """
        self.serverAddr = s_addr
        self.serverPort = int(s_port)
        self.secretPass = secret
        self.server = True
        
        self.output_obj = vpnOutput()
        self.keygen_obj = vpnKeygen(secret)
        self.encrypt_obj = vpnEncrypter(self.keygen_obj.getSymmetricKey())
        self.DH_public = self.keygen_obj.DH_object.publicKey
        self.DH_private = self.keygen_obj.DH_object.privateKey

        self.output_obj.debug("Server's Public DH Key(g^b mod p): " + str(self.DH_public))
        self.output_obj.debug("Server's Private DH Key(b): " + str(self.DH_private))
        
        self.serverSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serverSock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    def get_socket (self):
        """
        Fetch the server socket
        """
        return self.serverSock

    def guiRecv(self, stop_event):
        """
        Threaded function called from the GUI to poll and receive messages from server socket
        """
        while (not stop_event.is_set()):
            try:
                ready_to_read, ready_to_write, in_error = \
                    select.select([self.clientSock,], [self.clientSock,], [], 5)
            except select.error, msg:
                print (bcolors.fail +' Socket Closed. Error Code : ' + str(msg[0]) + ' Message : ' + msg[1] + bcolors.endc)
                self.output_obj.debug('Socket Closed. Error Code : ' + str(msg[0]) + ' Message : ' + msg[1])
                self.clientSock.shutdown(0)    # 0 = done receiving, 1 = done sending, 2 = both
                self.close_client()
            if len(ready_to_read) > 0:
                message =  self.s_receive_message()
                self.output_obj.message(str(message))

    def runServer(self):
        """
        runServer connects to client, authenticates the client, implements the session 
        key and then sends and receives messages until the user enters exit. 
        USED FOR DEBUGGING. Called from vpnTest.
        """
        self.prepare_socket(1)
        while self.server == True:
            self.accept_connection()
            if self.authenticate_client() == False:
                self.close_client()
            self.update_key(self.keygen_obj.getSessionKey())
            while self.connect == True: 
                message = raw_input("[Server Prompt]: ")
                if message == "exit":
                    self.connect = False
                else:
                    self.s_send_message(message)
                    self.s_receive_message()
            self.close_client()
        self.terminate()

    def authenticate_client(self):
        """
        Authenticates the client by sending client a nonce. If client is authorized, client
        will have the proper key to correctly encrypt the nonce. Server will compare the 
        received encrypted nonce, decrypt, and verify if the nonce matches.
        Establishes a session key with client using DeffieHellman key exchange method.
        """
        #print(bcolors.blue +"Authenticating the client"+bcolors.endc)
        self.output_obj.debug("Authenticate the client")
        self.receive_authentication_request()
        self.send_signature()
        if self.verify_authentication() == False:
            print(bcolors.fail+"Authentication failed. User is not authenticated."+bcolors.endc)
            return False
        #self.keygen_obj.setDH_otherKey(self.DH_other)
        self.output_obj.debug("Received Client Public DH: " + self.DH_other)
        self.keygen_obj.generateSessionKey(int(self.DH_other))
        self.output_obj.debug("New Shared Session Key: " +hexlify(self.keygen_obj.getSessionKey()))
        return True

    def receive_authentication_request(self):
        """
        Receives nonce from client.
        """
        print(bcolors.blue+"Receiving the Authentication Request ..."+bcolors.endc)
        self.output_obj.debug("Receiving the Authentication Request")
        received_data = self.receive_data()
        #print received_data
        self.client_nonce = received_data[:16]
        print("\tReceiving the Authentication Request")
        self.output_obj.debug('Received nonce= '+ hexlify(self.client_nonce))

    def send_signature(self):
        """
        Sends server DH public key and nonce to client.
        """
        print (bcolors.blue+"Sending Signature ..."+bcolors.endc)
        self.output_obj.debug("Sending Signature")
        self.server_nonce = self.encrypt_obj.generate_random_bytes(16)
        print ("\tServer nonce: "+hexlify(self.server_nonce))
        self.output_obj.debug("Server nonce:" + hexlify(self.server_nonce))
        print ("\tKey: "+ hexlify(self.encrypt_obj.get_key()))
        self.output_obj.debug("Key: "+ hexlify(self.encrypt_obj.get_key()))
        self.output_obj.debug("Send Server DH Public Key: "+ str(self.DH_public))
        plain_text = self.client_nonce + str(self.DH_public)
        mac = self.encrypt_obj.compute_HMAC(self.encrypt_obj.get_key(), plain_text)
        print ("\tSend MAC: " + hexlify(mac))
        self.output_obj.debug("Send MAC: " + hexlify(mac))
        iv = self.encrypt_obj.generate_random_bytes(16)
        print("\tSend IV: " + self.encrypt_obj.convert_to_hex(iv))
        self.output_obj.debug("Send IV: " + self.encrypt_obj.convert_to_hex(iv))
        ctt = self.encrypt_obj.encrypt_AES_CFB(plain_text, iv)
        print("\tSend Cipher Text: "+ self.encrypt_obj.convert_to_hex(ctt))
        self.output_obj.debug("Send Cipher Text: "+ self.encrypt_obj.convert_to_hex(ctt))
        self.send_data(iv + mac + self.server_nonce + ctt)

    def verify_authentication(self):
        """
        Receives nonce back from client and check that it is correct.
        """
        print (bcolors.blue+"Verifying the authentication..."+bcolors.endc)
        self.output_obj.debug("Verifying the Authentication")
        received_data = self.s_receive_message()
        received_nonce = received_data[:16]
        self.DH_other = received_data[16:]
        print ("\tAuthentication Plain Text (in HEX): " + str(binascii.hexlify( received_data)))
        self.output_obj.debug("Authentication Plain Text: " + str(received_data))
        print("\tReceived nonce: " + hexlify(received_nonce))
        self.output_obj.debug("Received nonce: " + hexlify(received_nonce))
        print("\tActual nonce: " + hexlify(self.server_nonce))
        self.output_obj.debug("Actual nonce: " + hexlify(self.server_nonce))
        if received_nonce == self.server_nonce:
            print(bcolors.green+"\t\tClient authenticated."+bcolors.endc)
            self.output_obj.debug("Client authenticated")
            return True
        else:
            print(bcolors.fail+"\t\tClient is NOT authenticated."+bcolors.endc)
            self.output_obj.debug("Client is NOT authenticated.")
            return False

    def update_key(self, new_key):
        """
        Updates the key used for encryption.
        """
        self.encrypt_obj = vpnEncrypter(new_key)  # secret should be hashed!

    def s_send_message(self, plain_text):
        """
        Encrypts plain text and sends message to client.
        """
        self.output_obj.debug("Send Encrypted Message")
        self.output_obj.debug("Key: "+ hexlify(self.encrypt_obj.get_key()))
        mac = self.encrypt_obj.compute_HMAC(self.encrypt_obj.get_key(), plain_text)
        self.output_obj.debug("Send MAC: " + hexlify(mac))
        iv = self.encrypt_obj.generate_random_bytes(16)
        self.output_obj.debug("Send IV: " + self.encrypt_obj.convert_to_hex(iv))
        ctt = self.encrypt_obj.encrypt_AES_CFB(plain_text, iv)
        self.output_obj.debug("Send Cipher Text: "+ self.encrypt_obj.convert_to_hex(ctt))
        self.send_data(iv + mac+ ctt)

    def s_receive_message(self):
        """
        Receives message from client and decrypts the cipher text.
        """
        self.output_obj.debug("Receive Encrypted Messages")
        received_data = self.receive_data()
        self.output_obj.debug("Received Data: " + self.encrypt_obj.convert_to_hex(received_data))
        iv = received_data[:16]
        ref_mac = received_data[16:48]
        self.output_obj.debug("Received MAC: " + hexlify(ref_mac))
        self.output_obj.debug("Received IV: " + self.encrypt_obj.convert_to_hex(iv))
        cipher_text = received_data[48:]
        self.output_obj.debug("Received Cipher Text: " + self.encrypt_obj.convert_to_hex(cipher_text))
        plain_text = self.encrypt_obj.decrypt_AES_CFB(cipher_text, iv)
        com_mac = self.encrypt_obj.compute_HMAC(self.encrypt_obj.get_key(), plain_text)
        self.output_obj.debug("Computed MAC: " + hexlify(com_mac))
        if self.encrypt_obj.compare_HMAC(ref_mac, com_mac):
            self.output_obj.debug("MACs match")
        else:
            self.output_obj.debug("MAC does not match! Message has been corrupted")
            #self.output_obj.debug("Message integrity not verified")
        self.output_obj.debug("Received Plain Text: " + str(plain_text))
        return plain_text

    def prepare_socket(self, numLinks):
        """
        Prepares the server socket.
        """
        print(bcolors.blue+"Preparing the server socket ..."+bcolors.endc)
        try:
            self.serverSock.bind((self.serverAddr, self.serverPort))
        except socket.error , msg:
            print(bcolors.fail+'Binding server socket failed. Error Code : ' + str(msg[0]) + ' Message : ' + msg[1]+bcolors.endc)
            self.output_obj.debug('Bind failed. Error Code : ' + str(msg[0]) + ' Message : ' + msg[1])
            self.terminate()
        print ('\tServer Socket bind complete')
        self.output_obj.debug('Server Socket bind complete')
        self.serverSock.listen(numLinks) 
        print ('\tServer Socket is now listening on Port: ' + str(self.serverPort))
        self.output_obj.debug('Server Socket is now listening on Port: ' + str(self.serverPort))

    def accept_connection(self):
        """
        Wait for client to connect to server socket.
        """
        #wait to accept a connection - blocking call
        try:
            self.clientSock, self.clientAddr = self.serverSock.accept()
        except socket.error, msg:
            self.output_obj.debug('Accept failed. Error Code : ' + str(msg[0]) + ' Message : ' + msg[1])
            self.terminate()
        self.connect = True
        self.output_obj.debug('Client ' + str(self.clientAddr) + ' has requested connection...')

    def send_data(self, data):
        """
        Sends data to the client through socket.
        """
        try:
            self.output_obj.debug('Sending : ' + hexlify(data))
            self.clientSock.sendall(data)
        except socket.error, msg:
            self.output_obj.debug('Send failed. Error Code : ' + str(msg[0]) + ' Message : ' + msg[1])
            self.terminate()

    def receive_data(self):
        """
        Receives data from the client through socket.
        """
        try:
            data = self.clientSock.recv(4096)
        except socket.error, msg:
            self.output_obj.debug('Receive failed. Error Code : ' + str(msg[0]) + ' Message : ' + msg[1])
            self.terminate()
        self.output_obj.debug('Received : ' + hexlify(data))
        return data

    def close_client(self):
        """
        Closes the client socket.
        """
        self.output_obj.debug('Closing connection with client ' + str(self.clientAddr))
        self.clientSock.close()
        self.connect = False
        self.clientSock = None
        self.clientAddr = None
        
    def terminate(self):
        """
        Terminates the server.
        """
        self.output_obj.debug('Closing server connection ' + str(self.serverAddr))
        self.serverSock.close()
        self.serverSock = None
        self.output_obj.debug('Terminating server')
        sys.exit()
        
        
class bcolors:
    header = '\033[95m'
    blue = '\033[94m'
    green = '\033[92m'
    warning = '\033[93m'
    fail = '\033[91m'
    red = '\033[91m'
    endc = '\033[0m'