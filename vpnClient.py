'''
Created on 2014-10-07

@author: Ehsan
@contributor: charlie
'''
from vpnEncrypter import vpnEncrypter
from vpnKeygen import vpnKeygen
from vpnOutput import vpnOutput
import select
import socket
from _socket import IPPROTO_TCP
from Crypto.Hash import SHA256
import sys
from binascii import hexlify
import sys
import os

class vpnClient(object):
    """
    Will be an object in the vpnGui class.
    
    This class is responsible for executing client functions.
    """
    keygen_obj = None
    encrypt_obj = None
    output_obj = None
    clientAddr = None
    clientPort = None
    clientSock = None
    serverAddr = None
    serverPort = None
    secretPass = None
    DH_private = None
    DH_public = None
    DH_other = None
    client_nonce = 0
    server_nonce = 0

    def __init__(self, c_addr, c_port, s_addr, s_port, secret):
        """
        vpnClient constructor.
            -    Stores the value entered from GUI
            -    Create and configure socket for server
            -    Create output, keygen, encrypter object
        """
        self.clientAddr = c_addr
        self.clientPort = int(c_port)
        self.serverAddr = s_addr
        self.serverPort = int(s_port)
        self.secretPass = secret

        self.output_obj = vpnOutput()
        self.keygen_obj = vpnKeygen(secret)
        self.encrypt_obj = vpnEncrypter(self.keygen_obj.getSymmetricKey())
        self.DH_public = self.keygen_obj.DH_object.publicKey
        self.DH_private = self.keygen_obj.DH_object.privateKey

        self.output_obj.debug("Client's Public DH Key(g^a mod p): " + str(self.DH_public))
        self.output_obj.debug("Client's Private DH Key(a): " + str(self.DH_private))

        self.clientSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
        self.clientSock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    def get_socket(self):
        """
        Fetch the client socket
        """
        return self.clientSock

    def guiRecv(self, stop_event):
        """
        Threaded function called from the GUI to poll and receive messages from client socket
        """
        while ( not stop_event.is_set() ):
            try:
                ready_to_read, ready_to_write, in_error = \
                    select.select([self.clientSock,], [self.clientSock,], [], 5)
            except select.error:
                self.clientSock.shutdown(0)    # 0 = done receiving, 1 = done sending, 2 = both
                self.terminate()
            if len(ready_to_read) > 0:
                message = self.s_receive_message()
                self.output_obj.message(message)

    def runClient(self):
        """
        runClient connects to server, authenticates the server, implements the session 
        key and then sends and receives messages until the user enters exit. 
        USED FOR DEBUGGING. Called from vpnTest.
        """
        self.prepare_socket()
        self.connect_server()
        if self.authenticate_server() == False:
            self.terminate()   
        self.update_key(self.keygen_obj.getSessionKey())
        while self.connect == True:
            message = raw_input("[Client Prompt]: ")
            if message == "exit":
                self.connect = False
            else:
                self.s_send_message(message)
                self.s_receive_message()
        self.terminate()

    # authenticates the server by checking that the server returns a nonce correctly and establishes the DH session key
    def authenticate_server(self):
        """
        Authenticates the server by sending server a nonce. If server is authorized, server
        will have the proper key to correctly encrypt the nonce. Client will compare the 
        received encrypted nonce, decrypt, and verify if the nonce matches.
        Establishes a session key with server using DeffieHellman key exchange method.
        """
        print ("Authenticating the server ...")
        self.output_obj.debug("Authenticating server")
        self.request_authentication()
        if self.verify_authentication() == False:
            self.output_obj.debug("server authentication failed")
            print("\tServer Authentication failed")
            return False
        print("\tServer Authenticated")
        self.send_signature()
        #self.keygen_obj.setDH_otherKey(self.DH_other)
        self.output_obj.debug("Received Server Public DH: " + self.DH_other)
        self.keygen_obj.generateSessionKey(int(self.DH_other))
        self.output_obj.debug("New Shared Session Key: " + hexlify(self.keygen_obj.getSessionKey()))
        self.connect = True
        return True

    def request_authentication(self):
        """
        Sends nonce to server.
        """
        print("Requesting the authentication ...")
        self.output_obj.debug("Requesting the Authentication")
        self.client_nonce = self.encrypt_obj.generate_random_bytes(16)
        print('\tnonce= ' + hexlify(self.client_nonce))
        self.send_data(self.client_nonce)

    def send_signature(self):
        """
        Sends client DH public key to server.
        """
        self.output_obj.debug("Send Client DH Public Key: "+ str(self.DH_public))
        self.s_send_message(self.server_nonce + str(self.DH_public))

    def verify_authentication(self):
        """
        Receives nonce back from server and check that it is correct.
        Also receives server nonce and server DH public key.
        """
        print("Verifying the authentication ...")
        self.output_obj.debug("Verifying the Authentication:")
        received_data = self.receive_data()
        print("\tReceived data: "+ self.encrypt_obj.convert_to_hex(received_data))
        self.output_obj.debug("Received Data: " + self.encrypt_obj.convert_to_hex(received_data))
        iv = received_data[:16]
        ref_mac = received_data[16:48]
        self.server_nonce = received_data[48:64]
        print ("\tReceived MAC"  + hexlify(ref_mac) )
        self.output_obj.debug("Received MAC: " + hexlify(ref_mac))
        print ("\tReceived IV: " + self.encrypt_obj.convert_to_hex(iv))
        self.output_obj.debug("Received IV: " + self.encrypt_obj.convert_to_hex(iv))
        cipher_text = received_data[64:]
        print ("\tReceived Cipher Text: " + self.encrypt_obj.convert_to_hex(cipher_text))
        self.output_obj.debug("Received Cipher Text: " + self.encrypt_obj.convert_to_hex(cipher_text))
        plain_text = self.encrypt_obj.decrypt_AES_CFB(cipher_text, iv)
        com_mac = self.encrypt_obj.compute_HMAC(self.encrypt_obj.get_key(), plain_text)
        print("\tComputed MAC: " + hexlify(com_mac))
        self.output_obj.debug("Computed MAC: " + hexlify(com_mac))
        self.encrypt_obj.compare_HMAC(ref_mac, com_mac)
        print("\tReceived Plain Text: " + str(plain_text))
        self.output_obj.debug("Received Plain Text: " + str(plain_text))
        received_nonce = plain_text[:16]
        self.DH_other = plain_text[16:]
        print("\tAuthenticate Plain Text: " + str(plain_text))
        print("\tReceived nonce: " + hexlify(received_nonce))
        print("\tActual nonce: " + hexlify(self.client_nonce))
        print("\tServer nonce: " + hexlify(self.server_nonce))
        self.output_obj.debug("Authenticate Plain Text: " + str(plain_text))
        self.output_obj.debug("Received nonce: " + hexlify(received_nonce))
        self.output_obj.debug("Actual nonce: " + hexlify(self.client_nonce))
        self.output_obj.debug("Server nonce: " + hexlify(self.server_nonce))
        if received_nonce == self.client_nonce:
            print ("\tConfirmed nonce. Server authenticated.")
            self.output_obj.debug("Server authenticated.")
            return True
        else:
            print("\tInvalid nonce. Server is NOT authenticated.")
            self.output_obj.debug("Server is NOT authenticated.")
            return False

    def update_key(self, new_key):
        """
        Updates the key used for encryption.
        """
        self.encrypt_obj = vpnEncrypter(new_key) #secret should be hashed!

    def s_send_message(self, plain_text):
        """
        Encrypts plain text and sends message to server.
        """
        self.output_obj.debug("Send encrypted message:")
        self.output_obj.debug("Key: " + hexlify(self.encrypt_obj.get_key()))
        mac = self.encrypt_obj.compute_HMAC(self.encrypt_obj.get_key(), plain_text)
        self.output_obj.debug("Send MAC: " + hexlify(mac))
        iv = self.encrypt_obj.generate_random_bytes(16)
        self.output_obj.debug("Send IV: " + self.encrypt_obj.convert_to_hex(iv))
        ctt = self.encrypt_obj.encrypt_AES_CFB(plain_text, iv)
        self.output_obj.debug("Send Cipher Text: "+ self.encrypt_obj.convert_to_hex(ctt))
        self.send_data(iv + mac + ctt)

    def s_receive_message(self):
        """
        Receives message from server and decrypts the cipher text.
        """
        self.output_obj.debug("Receive encrypted messages")
        received_data = self.receive_data()
        self.output_obj.debug("Received Data: " + self.encrypt_obj.convert_to_hex(received_data))
        iv = received_data[:16]
        ref_mac = received_data[16:48]
        self.output_obj.debug("Received MAC: " + hexlify(ref_mac))
        self.output_obj.debug("Received IV: " + self.encrypt_obj.convert_to_hex(iv))
        cipher_text = received_data[48:]
        self.output_obj.debug("Received Cipher Text: " + self.encrypt_obj.convert_to_hex(cipher_text))
        plain_text = self.encrypt_obj.decrypt_AES_CFB(cipher_text, iv)
        com_mac = self.encrypt_obj.compute_HMAC(self.encrypt_obj.get_key(), plain_text)
        self.output_obj.debug("Computed MAC: " + hexlify(com_mac))
        if self.encrypt_obj.compare_HMAC(ref_mac, com_mac):
            self.output_obj.debug("MACs match")
        else:
            self.output_obj.debug("MAC does not match! Message has been corrupted")
            #self.output_obj.debug("Message integrity not verified")
        self.output_obj.debug("Received Plain Text: " + str(plain_text))
        #self.output_obj.debug(str(plain_text))
        print (plain_text)
        return plain_text

    def prepare_socket(self):
        """
        Prepares the client socket.
        """
        try:
            self.clientSock.bind((self.clientAddr, self.clientPort))
        except socket.error, msg:
            self.output_obj.debug('Bind failed. Error Code : ' + str(msg[0]) + ' Message : ' + msg[1])
            self.terminate()
        self.output_obj.debug('Socket bind complete')

    def connect_server(self):
        """
        Connects to server socket.
        """
        #Connect to remote server
        try:
            self.clientSock.connect((self.serverAddr , self.serverPort))
        except socket.error, msg:
            self.output_obj.debug( 'Connect to server failed. Error Code : ' + str(msg[0]) + ' Message : ' + msg[1])
            self.terminate()
        self.output_obj.debug('Connected to ' + self.serverAddr )

    def send_data(self, data):
        """
        Sends data to server through socket.
        """
        try:
            self.output_obj.debug( 'Sending : ' + hexlify(data))
            self.clientSock.sendall(data)
        except socket.error, msg:
            self.output_obj.debug( 'Send failed. Error Code : ' + str(msg[0]) + ' Message : ' + msg[1])
            self.terminate()

    def receive_data(self):
        """
        Receives data from server through socket.
        """
        try:
            data = self.clientSock.recv(4096)
        except socket.error, msg:
            self.output_obj.debug( 'Receive failed. Error Code : ' + str(msg[0]) + ' Message : ' + msg[1])
            self.terminate()
        self.output_obj.debug( 'Received : ' + hexlify(data) )
        return data

    def terminate(self):
        """
        Terminates the client.
        """
        self.output_obj.debug('Closing connection with server ' + str(self.serverAddr))
        self.clientSock.close()
        self.clientSock = None
        self.output_obj.debug('Terminating client')
        sys.exit()

 
class bcolors:
    header = '\033[95m'
    blue = '\033[94m'
    green = '\033[92m'
    warning = '\033[93m'
    fail = '\033[91m'
    endc = '\033[0m'