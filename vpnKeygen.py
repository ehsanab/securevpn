'''
Created on 2014-10-08

@author: charlie
'''

from binascii import hexlify
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
import vpnDiffieHellman

class vpnKeygen(object):
    """
    Will be an object in the vpnClient and vpnServer class.
    
    This class is responsible for generating and storing the keys for the
    client and server class.
    """
    DH_object = None        #vpnDiffieHellman object
    DH_otherKey = None      #public key of other client/server
    
    secretPass = None       #shared secret value
    sessionKey = None       #symmetric session key generated from D-H key exchange
    symmetricKey = None     #symmetric key generated from shared secret value
    privateKey = None       #hashed exponent value a -or -b from D-H key exchange
    publicKey = None        #hased (g^a mod p) -or- (g^b mod p) from D-H key exchange
    
    def __init__(self, secret):
        """
        vpnKeygen constructor.
            -    Takes in the shared secret value as a pass in parameter.
            -    Generate the initial symmetric, private and public key 
                 upon construction.
            -    Creates vpnDiffieHellman object to facilitate key exchange.
        """
        self.secretPass = str(secret)
        self.DH_object = vpnDiffieHellman.vpnDiffieHellman()
        self.generateSymmetricKey()
        self.generatePublicKey()
        self.generatePrivateKey()

    def generateSymmetricKey(self):
        """
        Generate the initial symmetric key by SHA256 hashing the shared
        secret value.
        """
        key = SHA256.new()
        key.update(str(self.secretPass))
        #print key.hexdigest()
        self.symmetricKey = key.digest()
    
    def generateSessionKey(self, other):
        """
        Generate the symmetric session key using DiffieHellman key exchange.
        """
        self.DH_object.genSharedKey(other)
        self.sessionKey = self.DH_object.sharedKey
    
    def generatePublicKey(self):
        """
        Generate the public key by SHA256 hashing the D-H public key value.
        """
        key = SHA256.new()
        key.update(str(self.DH_object.publicKey))
        #print key.hexdigest()
        self.publicKey = key.digest()

    def generatePrivateKey(self):
        """
        Generate the private key by SHA256 hashing the D-H private key value.
        """
        key = SHA256.new()
        key.update(str(self.DH_object.privateKey))
        #print key.hexdigest()
        self.privateKey = key.digest()

    def getSymmetricKey(self):
        """
        Fetch the initial symmetrical key.
        """
        return self.symmetricKey
    
    def getSessionKey(self):
        """
        Fetch the symmetrical session key.
        """
        return self.sessionKey
               
    def getPublicKey(self):
        """
        Fetch the public key.
        """
        return self.publicKey
    
    def getPrivateKey(self):
        """
        Fetch the private key.
        """
        return self.privateKey
    
    def getDH_publicKey(self):
        """
        Fetch the other client/server public key used during D-H exchange.
        """
        return self.DH_object.publicKey
    
    def setDH_otherKey(self, otherkey):
        """
        Sets the other client/server public key used during D-H exchange.
        """
        self.DH_otherKey = otherkey

#Section below was used for testing and debugging       
"""            
if __name__=="__main__":
    
    a = vpnKeygen("test")
    b = vpnKeygen("test")
     
    a.setDH_otherKey(b.DH_object.publicKey)
    a.generateSessionKey()
    #b.genSharedKey(a.vpnDiffieHellman.publicKey)
    
    print "DH Public Key:", a.getDH_publicKey()
    print "Symmetric Key:", hexlify(a.symmetricKey)
    print "Session Key:", hexlify(a.sessionKey)
    print "Public Key:", hexlify(a.publicKey)
    print "Private Key:", hexlify(a.privateKey)
"""