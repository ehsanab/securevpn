'''
Created on 2014-10-08

@author: charlie
'''

class vpnOutput(object):
    """
    Will be an object in the vpnClient and vpnServer class.
    
    This class is responsible for storing the buffer of debug and output
    messages for the GUI.
    """
    debugOutput = None
    messageOutput = None

    def __init__(self):
        """
        vpnOutput constructor.
            -    Creates the buffer for debug and output
        """
        self.debugOutput = []
        self.messageOutput = []
        
    def debug(self, msg):
        """
        Function populates the debug list buffer
        """
        self.debugOutput.append(msg)
    
    def message(self, msg):
        """
        Function populates the output list buffer
        """
        self.messageOutput.append(msg) 
   
    def del_debug(self):
        """
        Function clears the debug list buffer. Preferably called AFTER
        debug messages are printed.
        """
        del self.debugOutput[:]
        
    def del_message(self):
        """
        Function clears the output list buffer. Preferably called AFTER
        output messages are printed.
        """
        del self.messageOutput[:]
    
    def check_debug(self):
        """
        Function checks if there are any entries in debug buffer for
        printing. Returns True when buffer has content and False otherwise.
        """
        if len(self.debugOutput) > 0:
            return True
        else:
            return False
    
    def check_message(self):
        """
        Function checks if there are any entries in output buffer for
        printing. Returns True when buffer has content and False otherwise.
        """
        if len(self.messageOutput) > 0:
            return True
        else:
            return False
    
    def print_debug(self):
        """
        Function prints all the content in the debug buffer.
        """
        print('\n'.join(map(str, self.debugOutput)))
        
    def print_message(self):
        """
        Function prints all the content in the output buffer.
        """
        print('\n'.join(map(str, self.messageOutput)))
        
    def get_debug(self):
        """
        Function fetches the debug buffer.
        """
        return self.debugOutput    
    
    def get_message(self):
        """
        Function fetches the output buffer.
        """
        return self.messageOutput

#Section below was used for testing and debugging       
"""        
if __name__=="__main__":
    
    obj = vpnOutput()
    obj.debug("LOH LOHGSDLFJS:LFJ:SLDJF")
    obj.debug("LOH LOHGSDLFJS:LFJ:SLDJF")
    obj.debug("LOH LOHGSDLFJS:LFJ:SLDJF")
    obj.debug("LOH LOHGSDLFJS:LFJ:SLDJF")
    obj.debug("LOH LOHGSDLFJS:LFJ:SLDJF")
    obj.debug("LOH LOHGSDLFJS:LFJ:SLDJF")
    obj.debug("LOH LOHGSDLFJS:LFJ:SLDJF")
    obj.debug("LOH LOHGSDLFJS:LFJ:SLDJF")
    obj.debug("LOH LOHGSDLFJS:LFJ:SLDJF")
    obj.debug("LOH LOHGSDLFJS:LFJ:SLDJF")
    obj.debug("LOH LOHGSDLFJS:LFJ:SLDJF")
    
    if obj.check_debug() == True:
        obj.print_debug()
    else:
        print "nothing."
    obj.del_debug()
    if obj.check_debug() == True:
        obj.print_debug()
    else:
        print "nothing."
"""